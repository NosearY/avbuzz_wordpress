<?php get_header(); ?>
<div class="container">
	<div class="row">
		<?php get_search_form(); ?>
	</div>
	<div class="row searchresult"> 
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
						<div class="img-box">
								<div class="article-title article-title-font">
									<article>
											<?php the_title();?>
									</article>
								</div>
								<div class="article-img">
									<?php
										if(has_post_thumbnail()){
											the_post_thumbnail();
										}
									?>
								</div>
						</div>
			<?php endwhile; ?>
		<?php else : ?>
			<article class="article-content">
				<h1>搜尋結果</h1>
				<p>很抱歉，找不到你所搜尋的文章，你可以試著用其他關鍵字再次搜尋。</p>
				
			</article>
		<?php endif; ?>
		<?php wp_pagenavi(); ?>
	</div>
</div>
<?php get_footer(); ?>