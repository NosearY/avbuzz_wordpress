<!DOCTYPE html>
<?php avbuzz_enqueue_assets_forarticle();?>
<?php if(function_exists('bac_PostViews')) {
    bac_PostViews(get_the_ID());
}?>
<html <?php language_attributes(); ?>>

<head>
<?php wp_head(); ?>
<meta charset="<?php bloginfo('charset');?>" />
<!-- <meta name="viewport" content="width=device-width, initial-scale=1" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico?v=2" />
<style type="text/css">
html {text-align:center;}

#cssmenu {
  background: #9BABFF;
  width:100%;
  margin-top: 10px;
  margin-left:auto;
  margin-right:auto;
}
#cssmenu ul {
  list-style: none;
  margin: 0;
  padding: 0;
  line-height: 1;
  display: block;
  zoom: 1;
}
#cssmenu ul:after {
  content: " ";
  display: block;
  font-size: 0;
  height: 0;
  clear: both;
  visibility: hidden;
}
#cssmenu ul li {
  display: inline-block;
  padding: 0;
  margin: 0;
}
#cssmenu.align-right ul li {
  float: right;
}
#cssmenu.align-center ul {
  text-align: center;
}
#cssmenu ul li a {
  color: #ffffff;
  text-decoration: none;
  display: block;
  padding: 11px 15px;
  font-family: 'Open Sans', sans-serif;
  font-weight: 700;
  font-size:1rem;
  text-transform: uppercase;
  position: relative;
  -webkit-transition: color .25s;
  -moz-transition: color .25s;
  -ms-transition: color .25s;
  -o-transition: color .25s;
  transition: color .25s;
}
#cssmenu ul li a:hover {
  color: #333333;
}
#cssmenu ul li a:hover:before {
  width: 100%;
}
#cssmenu ul li a:after {
  content: "";
  display: block;
  position: absolute;
/*  right: -3px;
  top: 15px;*/
  height: 6px;
  width: 6px;
  background: #ffffff;
  opacity: .5;
}
#cssmenu ul li a:before {
  content: "";
  display: block;
  position: absolute;
  left: 0;
  bottom: 0;
  height: 3px;
  width: 0;
  background: #333333;
  -webkit-transition: width .25s;
  -moz-transition: width .25s;
  -ms-transition: width .25s;
  -o-transition: width .25s;
  transition: width .25s;
}
#cssmenu ul li.last > a:after,
#cssmenu ul li:last-child > a:after {
  display: none;
}
#cssmenu ul li.active a {
  color: #333333;
}
#cssmenu ul li.active a:before {
  width: 100%;
}
#cssmenu.align-right li.last > a:after,
#cssmenu.align-right li:last-child > a:after {
  display: block;
}
#cssmenu.align-right li:first-child a:after {
  display: none;
}
.main_nav {
	text-align:center;
	background-color: #9BABFF;
}
</style>


<title><?php the_title(); ?></title>

</head>

<body background="<?php bloginfo('template_directory') ?>/images/background_Logo.jpg">

<article class="article-container">
 <?php while ( have_posts() ) : the_post();?>
 		<div class="article-header">
 			<p id="main-title"><?php the_title(); ?></p>
 			<p id="sub-title"><?php echo get_the_date('發佈日期: Y年 m月 d日'); ?></p>
 		</div>
 		<button class="article-button isTW" id="translateButton">
 		</button>
 		<div class="article-separator"><img src="<?php bloginfo('template_url'); ?>/images/separator.gif"/><img src="<?php bloginfo('template_url'); ?>/images/separator.gif"/></div>
					<div class="article-content">
					<?php the_content();?>
					</div>
			<?php
				endwhile;
			?>
				<div class="article-separator"><img src="<?php bloginfo('template_url'); ?>/images/separator.gif"/><img src="<?php bloginfo('template_url'); ?>/images/separator.gif"/></div>
				<?php
					function tomainpage(){
						get_site_url();
					}
				?>
				<button class="article-button" id="toHomepageButton" onclick="location.href='<?php echo get_home_url();?>'">
				</button>
</article>
				<div id="cssmenu" class="main_nav">
	<ul>
	   <li><a href='http://www.avbuzz.com/visithome/Buzz_One/Buzzlist.htm'><span>巴斯是誰</span></a></li>
	   <li><a href='http://www.avbuzz.com/new_home/w/ablist2004/buzzab2004b.htm'><span>巴斯相簿</span></a></li>
     <li><a href='http://www.avbuzz.com/sclist.htm'><span>貼文重溫</span></a></li>
     <li><a href='http://www.avbuzz.com/yesun/list.htm'><span>Yesun手稿</span></a></li>
     <li><a href='http://www.avbuzz.com/audio-video/200509/topgun/index.htm'><span>TopGun手記</span></a></li>
     <li><a href='http://www.avbuzz.com/audio-video/200804/hv2008/list50.htm'><span>家訪庫</span></a></li>
     <li><a href='http://www.avbuzz.com/reporterlist.htm'><span>聯絡我們</span></a></li>
	</ul>
</div>
				<br>
				<p align="center"><font color="#008000">各師兄與朋友，這篇文章，如非商業用途，歡迎轉貼再轉貼，如需借圖片使用，<br>
					<br>
					請自便，但敬請不要抹去圖中的 avbuzz.com簽名字樣，感激感激再感激。</font></p>
          <br>
					<p align="center"><font color="#ff9900">◆ </font><span style="color: teal"><font color="teal" size="2">&nbsp;</font></span>
						<font color="#ff9900">avbuzz.com精神，人人毒我，我毒人人 ◆</font></p>
          <br>
<?php get_footer(); ?>
