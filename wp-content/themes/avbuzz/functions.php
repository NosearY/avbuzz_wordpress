<?php
//adding thumbnail support
add_theme_support( 'post-thumbnails' ); 
add_action('admin_head', 'my_custom_fonts');
set_post_thumbnail_size( 150, 150 );
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

//turn off also compression
add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );

/**
 * Initialize pot meta
 * @return [type]
 */
function init_post_metas(){
	global $wpdb;
	$Posts = $wpdb->get_results("SELECT ID FROM " . $wpdb->posts);
	foreach ($Posts as $post)
	{
		//metadata_exists('post',$post->ID,)
	    if(!(get_post_meta($post->ID, 'priority', true)))
	    {
	        add_post_meta( $post->ID, 'priority', '0', true );
	    }
	}
}

/**
 * Custom wordpress navigation
 * @return [HTML element]
 */
function wp_pagenavi() {

global $wp_query;

$big = 999999999; // need an unlikely integer

$pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => min( -1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'show_all' => true,
        'prev_next' => false
    ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<div class="wp-pagenavi">';
        foreach ( $pages as $page ) {
                echo  $page ;
        }
       echo '</div>';
        }
}

function avbuzz_article_footer(){

}

/**
 * Enqueue assets
 * @return [type]
 */
function avbuzz_enqueue_assets(){
	//equeue scripts
	wp_enqueue_script('jquery' , get_template_directory_uri() . '/js/jquery-1.11.3.min.js');
	wp_enqueue_script('bootstrapminjs' , get_template_directory_uri() . '/js/bootstrap.js');
	wp_enqueue_script('script' , get_template_directory_uri() . '/js/script.js');
	wp_enqueue_style('hvrcss',get_template_directory_uri() . '/css/hover-min.css');
	//enqueue style sheets
	wp_enqueue_style('bsmincss' , get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('bsthememincss' , get_template_directory_uri() . '/css/bootstrap-theme.css');
	wp_enqueue_style('style' , get_template_directory_uri() . '/style.css');
	wp_enqueue_script( 'ajax-pagination', get_stylesheet_directory_uri() . '/js/ajax-pagination.js', array( 'jquery' ), '1.0', true );
	global $wp_query;
	wp_localize_script( 'ajax-pagination', 'ajaxpagination', array(
	'ajaxurl' => admin_url( 'admin-ajax.php' ),
	'query_vars' => json_encode( $wp_query->query )
	));
}

/**
 * Enqueue boostrap
 * @return [type] [description]
 */
function avbuzz_enqueue_bs(){
	wp_enqueue_style('bsmincss' , get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('bsthememincss' , get_template_directory_uri() . '/css/bootstrap-theme.css');
	wp_enqueue_script('bootstrapminjs' , get_template_directory_uri() . '/js/bootstrap.js');
}
/**
 * Enqueue assets for admin page
 * @return [type]
 */
function avbuzz_enqueue_assets_adminpage(){
	wp_enqueue_style('adminstyle' , get_template_directory_uri() . '/admin-style.css');
}

/**
 * Enqueue assets for article only
 * @return [type]
 */
function avbuzz_enqueue_assets_forarticle(){
	//equeue scripts
	wp_enqueue_script('jquery' , get_template_directory_uri() . '/js/jquery-1.11.3.min.js');
	wp_enqueue_script('script' , get_template_directory_uri() . '/js/script.js');
	wp_enqueue_script('jquery.s2t' , get_template_directory_uri() . '/js/jquery.s2t.js');	
	wp_enqueue_style('article' , get_template_directory_uri() . '/css/article.css');
}

function avbuzz_ajax_pagination(){

}


/**
 * Attach post permalink to thumbnail
 * @param  permalink id
 * @param  thumbnail id
 * @return HTML
 */
function thumbnail_withlink($permalink, $thumbnail){
	$html = '<a href="';
	$html .= $permalink;
	$html .= '" />';
	$html .= $thumbnail;	
	$html .= '</a>';										
	return $html;																			
}


/**
 * Attach post permalink to thumbnail
 * @param  [type]
 * @param  [type]
 * @param  [type]
 * @return [type]
 */
function my_post_image_html( $html, $post_id, $post_image_id ) {
		
	$perm = get_permalink($post_id);
	$post_keys = array(); $post_val = array();
	$post_keys = get_post_custom_keys($post_id);
	$hasURL = false;
	if (!empty($post_keys)) {
		foreach ($post_keys as $pkey) {
			if ($pkey=='url') {
				$post_val = get_post_custom_values($pkey);
			}
		}
		if (empty($post_val)) {
			$link = $perm;
		} else {
			$link = $post_val[0];
			$hasURL = true;
		}
	} else {
		$link = $perm;
	}
	/**/
	if($hasURL){
		$html = '<a id = "' . $post_id . '"href="' . $link . '" target="_blank" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';
	}
	//NO url, no id
	{
		$html = '<a href="' . $link . '" target="_blank" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';
	}
	return $html;
}

/**
 * Get ad object from BD
 * @param  [type]
 * @return [type]
 */
function get_ads_from_db($adid){
	global $wpdb;
	$cust_upload_path = '/avbuzz-uploads/';
	$result = $wpdb->get_row('select * from wp_ads where adid = '.$adid); 
	$adobject['adinturl']=content_url() . $cust_upload_path  .$result->adinturl;
	$adobject['adintref']=$result->adinturl;
	$adobject['adexturl']=$result->adexturl;
	return $adobject;
}

/**
 * Get raw post data from DB
 * @param  [type]
 * @return [type]
 */
function get_raw_post_data($postid){
	global $wpdb;
	$result = $wpdb->get_row('select post_content from wp_posts where id = '.$postid); 
	return $result;
}

/**
 * Refresh after saving ad result
 * @param  [type]
 * @return [type]
 */
function my_refresh($location){
	echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
	exit;
}

/**
 * Insert HTML embbeded style
 * @return [type]
 */
function my_custom_fonts() {
  echo '<style>
    .form-table img{
    	width:140px;
    	height:170px;
    }

	.img-form-middle th{
    	padding:15px 10px;
    }

    .img-form-middle img{
    	width: 232px;
		height:153px;
    }
  </style>';
}

/**
 * Ajax post pagination
 * @return [type]
 */
/*function my_get_posts_for_pagination() {
	$paged = $_GET['page']; // Page number
	$html = '';
	$pag = 0;
	if( filter_var( intval( $paged ), FILTER_VALIDATE_INT ) ) {
		$pag = $paged;
		$args = array(
			'paged' => $pag, // Uses the page number passed via AJAX
			'posts_per_page' => 8 // Change this as you wish
		);
		$loop = new WP_Query( $args );
			
		if( $loop->have_posts() ) {
			while( $loop->have_posts() ) {
				$loop->the_post();
				// Build the HTML string with your post's contents
			}
				
			wp_reset_query();
		}
	}
		
	echo $html;
	exit();

}*/

function my_get_posts( $query )
{
    if (! is_admin() && $query->is_main_query()){
        
    }
	
}

/**
 * Get head flash AD code from php template
 * @param  [type]
 * @return [type]
 */
function get_ad_template( $name = null ) {
	do_action( 'get_footer', $name );

	$templates = array();
	$name = (string) $name;
	if ( '' !== $name )
		$templates[] = "footer-{$name}.php";

	$templates[] = '../../avbuzz-uploads/AD/ad.php';

	// Backward compat code will be removed in a future release
	if ('' == locate_template($templates, true))
		load_template( ABSPATH . WPINC . '/theme-compat/footer.php');
}


add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );

add_action( 'wp_ajax_nopriv_my_ajax_viewcount', 'my_ajax_viewcount' );
add_action( 'wp_ajax_my_ajax_viewcount', 'my_ajax_viewcount' );

function my_ajax_viewcount(){
	$postid =$_POST['postid'];

	bac_PostViews($postid); 
	exit();
}

/**
 * Ajax pagination
 * @return [type]
 */
function my_ajax_pagination() {
	$query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
	$query_vars['paged'] = $_POST['page'];
	$query_vars['post_status'] = 'publish';
	$query_vars['meta_query']  = array(
			'relation' => 'OR',
			array(
				'key'=>'priority',
				'compare'=>'NOT EXISTS',
				'value'=>'bugfix'),
			array(
				'key'=>'priority',
				'compare'=>'EXISTS',)
			);
	$query_vars['posts_per_page'] = 48;
	$query_vars['meta_key'] = 'priority';
	$query_vars['orderby'] = array('meta_value'=>'DESC','date'=>'DESC');
	$posts = new WP_Query( $query_vars );
	$GLOBALS['wp_query'] = $posts;
	add_filter( 'editor_max_image_size', 'my_image_size_override' );
		if( ! $posts->have_posts() ) {
			get_template_part( 'content', 'none' );
		}
		else {
		while ( $posts->have_posts() ) {
			$posts->the_post();
			$html.= '<div class="img-box hvr-glow">';
			$html.= '<div class="article-title article-title-font">';
			$html.= '<article>';
			$html.=  get_the_title();
			$html.=	'</article>';
			$html.=	'</div>';
			$html.=	get_the_post_thumbnail();
			$html.=	'</div>';
			
		}
		}
	remove_filter( 'editor_max_image_size', 'my_image_size_override' );
	echo $html;
	exit();
}

function my_image_size_override() {
return array( 825, 510 );
}

/**
 * External link from post meta
 * @return [type]
 */
/*function print_post_title() {
	global $post;
	$thePostID = $post->ID;
	$post_id = get_post($thePostID);
	$title = $post_id->post_title;
	$perm = get_permalink($post_id);
	$post_keys = array(); $post_val = array();
	$post_keys = get_post_custom_keys($thePostID);

	if (!empty($post_keys)) {
		foreach ($post_keys as $pkey) {
			if ($pkey=='url' || $pkey=='title_url' || $pkey=='url_title') {
				$post_val = get_post_custom_values($pkey);
			}
		}
		if (empty($post_val)) {
			$link = $perm;
		} else {
			$link = $post_val[0];
		}
	} else {
	$link = $perm;
	}
	echo '<h2><a id="1" href="'.$link.'" target="_blank" rel="bookmark" title="'.$title.'">'.$title.'</a></h2>';
}*/

/**
 * Sort post by priority DESC, date DESC
 * @param  [type]
 * @return [type]
 */
function custom_post_query_order($query){

	if (! is_admin() && $query->is_main_query()){
		$query->set('meta_query',
			array(
			'relation' => 'OR',
			array(
				'key'=>'priority',
				'compare'=>'NOT EXISTS',
				'value'=>'bugfix'),
			array(
				'key'=>'priority',
				'compare'=>'EXISTS',)
			)
		);
		$query->set('post_status',array('publish','future'));
		$query->set('posts_per_page',48);
		$query->set('meta_key','priority');
		$query->set('orderby',array('meta_value'=>'DESC','date'=>'DESC'));
	}
}

add_action('pre_get_posts','custom_post_query_order');

/*
* Set default priority
*/
/*add_action('publish_page', 'add_custom_field_automatically');
add_action('publish_post', 'add_custom_field_automatically');
add_action('publish_future_post', 'add_custom_field_automatically');*/

add_action( 'save_post', 'add_custom_field_automatically', 10, 3 );

function add_custom_field_automatically($post_id,$post, $update) {
	global $wpdb;
	$update = false;
	if(!wp_is_post_revision($post_id)) {
		add_post_meta($post_id, 'priority', '50', true);
	}
}

//Gets the  number of Post priority to be used later.
function get_PostPriority($post_ID){
    $priority_key = 'priority';
    //Returns values of the custom field with the specified key from the specified post.
    $priority = get_post_meta($post_ID, $priority_key, true);
 	//modified by Reason
    return $priority == ''? 0: $priority;
}
 
//Function that Adds a 'priority' Column to your Posts tab in WordPress Dashboard.
function post_column_priority($newcolumn){
    //Retrieves the translated string, if translation exists, and assign it to the 'default' array.
    $newcolumn['post_priority'] = __('Priority');
    return $newcolumn;
}
 
//Function that Populates the 'priority' Column with the number of views count.
function post_custom_column_priority($column_name, $id){
     
    if($column_name === 'post_priority'){
        // Display the Post View Count of the current post.
        // get_the_ID() - Returns the numeric ID of the current post.
        echo get_PostPriority(get_the_ID());
    }
}
//Hooks a function to a specific filter action.
//applied to the list of columns to print on the manage posts screen.
add_filter('manage_posts_columns', 'post_column_priority');
 
//Hooks a function to a specific action. 
//allows you to add custom columns to the list post/custom post type pages.
//'10' default: specify the function's priority.
//and '2' is the number of the functions' arguments.
add_action('manage_posts_custom_column', 'post_custom_column_priority',10,2);

//Function: Register the 'Views' column as sortable in the WP dashboard.
function register_post_column_priority_sortable( $newcolumn ) {
    $newcolumn['post_priority'] = 'post_priority';
    return $newcolumn;
}
add_filter( 'manage_edit-post_sortable_columns', 'register_post_column_priority_sortable' );
 
//Function: Sort Post Views in WP dashboard based on the Number of Views (ASC or DESC).
function sort_priority_column( $vars ) 
{
    if ( isset( $vars['orderby'] ) && 'post_priority' == $vars['orderby'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => 'priority', //Custom field key
            'orderby' => 'meta_value_num') //Custom field value (number)
        );
    }
    return $vars;
}
add_filter( 'request', 'sort_priority_column' );


/************************************CODE-1***************************************
* @Author: Boutros AbiChedid 
* @Date:   January 16, 2012
* @Websites: http://bacsoftwareconsulting.com/ ; http://blueoliveonline.com/
* @Description: Displays the Number of times Posts are Viewed on Your Blog.
* Function: Sets, Tracks and Displays the Count of Post Views (Post View Counter)
* Code is browser and JavaScript independent.
* @Tested on: WordPress version 3.2.1 
*********************************************************************************/
//Set the Post Custom Field in the WP dashboard as Name/Value pair 
function bac_PostViews($post_ID) {
 
    //Set the name of the Posts Custom Field.
    $count_key = 'post_views_count'; 
     
    //Returns values of the custom field with the specified key from the specified post.
    $count = get_post_meta($post_ID, $count_key, true);
     
    //If the the Post Custom Field value is empty. 
    if($count == ''){
        $count = 0; // set the counter to zero.
         
        //Delete all custom fields with the specified key from the specified post. 
        delete_post_meta($post_ID, $count_key);
         
        //Add a custom (meta) field (Name/value)to the specified post.
        add_post_meta($post_ID, $count_key, '0');
        return $count . ' View';
     
    //If the the Post Custom Field value is NOT empty.
    }else{
        $count++; //increment the counter by 1.
        //Update the value of an existing meta key (custom field) for the specified post.
        update_post_meta($post_ID, $count_key, $count);
         
        //If statement, is just to have the singular form 'View' for the value '1'
        if($count == '1'){
        return $count . ' View';
        }
        //In all other cases return (count) Views
        else {
        return $count . ' Views';
        }
    }
}

/*********************************CODE-3********************************************
* @Author: Boutros AbiChedid 
* @Date:   January 16, 2012
* @Websites: http://bacsoftwareconsulting.com/ ; http://blueoliveonline.com/
* @Description: Adds a Non-Sortable 'Views' Columnn to the Post Tab in WP dashboard.
* This code requires CODE-1(and CODE-2) as a prerequesite.
* Code is browser and JavaScript independent.
* @Tested on: WordPress version 3.2.1
***********************************************************************************/
 
//Gets the  number of Post Views to be used later.
function get_PostViews($post_ID){
    $count_key = 'post_views_count';
    //Returns values of the custom field with the specified key from the specified post.
    $count = get_post_meta($post_ID, $count_key, true);
 	//modified by Reason
    return $count == ''? 0: $count;
}
 
//Function that Adds a 'Views' Column to your Posts tab in WordPress Dashboard.
function post_column_views($newcolumn){
    //Retrieves the translated string, if translation exists, and assign it to the 'default' array.
    $newcolumn['post_views'] = __('Views');
    return $newcolumn;
}
 
//Function that Populates the 'Views' Column with the number of views count.
function post_custom_column_views($column_name, $id){
     
    if($column_name === 'post_views'){
        // Display the Post View Count of the current post.
        // get_the_ID() - Returns the numeric ID of the current post.
        echo get_PostViews(get_the_ID());
    }
}
//Hooks a function to a specific filter action.
//applied to the list of columns to print on the manage posts screen.
add_filter('manage_posts_columns', 'post_column_views');
 
//Hooks a function to a specific action. 
//allows you to add custom columns to the list post/custom post type pages.
//'10' default: specify the function's priority.
//and '2' is the number of the functions' arguments.
add_action('manage_posts_custom_column', 'post_custom_column_views',10,2);

//Function: Register the 'Views' column as sortable in the WP dashboard.
function register_post_column_views_sortable( $newcolumn ) {
    $newcolumn['post_views'] = 'post_views';
    return $newcolumn;
}
add_filter( 'manage_edit-post_sortable_columns', 'register_post_column_views_sortable' );
 
//Function: Sort Post Views in WP dashboard based on the Number of Views (ASC or DESC).
function sort_views_column( $vars ) 
{
    if ( isset( $vars['orderby'] ) && 'post_views' == $vars['orderby'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => 'post_views_count', //Custom field key
            'orderby' => 'meta_value_num') //Custom field value (number)
        );
    }
    return $vars;
}
add_filter( 'request', 'sort_views_column' );

add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );