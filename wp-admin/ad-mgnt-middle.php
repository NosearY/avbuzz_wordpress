<?php
/**
 * General settings administration panel.
 *
 * @package WordPress
 * @subpackage Administration
 */

/** WordPress Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

/** WordPress Translation Install API */
require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

require_once( ABSPATH . 'wp-admin/admin-header.php' );


if ( ! current_user_can( 'manage_options' ) )
	wp_die( __( 'You do not have sufficient permissions to manage options for this site.' ) );

?>

<div class="wrap">
<h2>Middle Image Management</h2>
<form method="post" action="" novalidate="novalidate">
<!-- 	<p class="submit">
	<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
	</p> -->
				<table class="form-table img-form-middle">
						  <tr>
						    <th>
								<a href="<?php $adobject = get_ads_from_db(8);echo $adobject['adexturl']; ?>">
									<img src="<?php  $adobject = get_ads_from_db(8);echo $adobject['adinturl'];?>"/>
								</a>
							</th>
						</tr>
						<tr>
							<td>
								<input name="adinturl8" type="text" id="adinturl8" aria-describedby="tagline-description" value="<?php  $adobject = get_ads_from_db(8);echo $adobject['adintref'];?>"
class="regular-text" />
								<p class="description" id="tagline-description">Internal Image URL</p>
							</td>													
						</tr>
						<tr>
							<td>
								<input name="adexturl8" type="text" id="adexturl8" aria-describedby="tagline-description" value="<?php  $adobject = get_ads_from_db(8);echo $adobject['adexturl'];?>" class="regular-text"/>
								<p class="description" id="tagline-description">External Image Redirect URL</p>
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" name="submit8" id="submit8" class="button button-primary" value="Refresh">	
						    </td>
						</tr>
					</table>
</form>
<?php
	global $wpdb;
	if(isset($_POST['submit8'])){
		$inturl = $_POST['adinturl8'];
		$exturl = $_POST['adexturl8'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 8));
		/*header('Location: '.$_SERVER['REQUEST_URI']);*/
		/*header("Location: wp-admin/ad-mgnt.php");*/
		my_refresh($_SERVER['REQUEST_URI']);
	}
/*	if(isset($_POST['submit9'])){
		$inturl = $_POST['adinturl9'];
		$exturl = $_POST['adexturl9'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 9));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit10'])){
		$inturl = $_POST['adinturl10'];
		$exturl = $_POST['adexturl10'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 10));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit11'])){
		$inturl = $_POST['adinturl11'];
		$exturl = $_POST['adexturl11'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 11));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit12'])){
		$inturl = $_POST['adinturl12'];
		$exturl = $_POST['adexturl12'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 12));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit6'])){
		$inturl = $_POST['adinturl6'];
		$exturl = $_POST['adexturl6'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 6));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit7'])){
		$inturl = $_POST['adinturl7'];
		$exturl = $_POST['adexturl7'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 7));
		my_refresh($_SERVER['REQUEST_URI']);
	}*/

?>

</div>