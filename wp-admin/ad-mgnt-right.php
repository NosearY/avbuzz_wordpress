<?php
/**
 * General settings administration panel.
 *
 * @package WordPress
 * @subpackage Administration
 */

/** WordPress Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

/** WordPress Translation Install API */
require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

require_once( ABSPATH . 'wp-admin/admin-header.php' );


if ( ! current_user_can( 'manage_options' ) )
	wp_die( __( 'You do not have sufficient permissions to manage options for this site.' ) );

?>

<div class="wrap">
<h2>Right Side AD Management</h2>
<form method="post" action="" novalidate="novalidate">
<!-- 	<p class="submit">
	<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
	</p> -->
<table class="form-table" action="<?php $_PHP_SELF ?>">
	<?php for($i = 11; $i<=18; $i++)
	{
	$adobject = get_ads_from_db($i);
	$html = '<tr >';
	$html .='<th >';
	$html .= '<a href="'. $adobject['adexturl'] . '">';
	$html .= '<img class="" src="' . $adobject['adinturl'].'"/>';
	$html .=	'</a>';
	$html .=	'</th>';
	$html .=	'<td><input name="adinturl'.$i.'" type="text" id="adinturl'.$i.'" aria-describedby="tagline-description" value="' .  $adobject['adintref'] .'"';
	$html .= ' class="regular-text" />';
	$html .=	'<p class="description" id="tagline-description">Internal Image URL</p></td>';
	$html .=	'<td><input name="adexturl'.$i.'" type="text" id="adexturl'.$i.'" aria-describedby="tagline-description" value="'.$adobject['adexturl'].'" class="regular-text" />';
	$html .=	'<p class="description" id="tagline-description">External Image Redirect URL</p></td>';
	$html .= '<td>';
	$html .= '<input type="submit" name="submit'.$i.'" id="submit'.$i.'" class="button button-primary" value="Refresh">';
	$html .= '</td>';
	$html .= '</tr>';
	echo $html;
	}
	?>
	</table>
</form>
<?php
	global $wpdb;
		if(isset($_POST['submit11'])){
		$inturl = $_POST['adinturl11'];
		$exturl = $_POST['adexturl11'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 11));
		/*header('Location: '.$_SERVER['REQUEST_URI']);*/
		/*header("Location: wp-admin/ad-mgnt.php");*/
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit12'])){
		$inturl = $_POST['adinturl12'];
		$exturl = $_POST['adexturl12'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 12));
		/*header('Location: '.$_SERVER['REQUEST_URI']);*/
		/*header("Location: wp-admin/ad-mgnt.php");*/
		my_refresh($_SERVER['REQUEST_URI']);
	}
	if(isset($_POST['submit13'])){
		$inturl = $_POST['adinturl13'];
		$exturl = $_POST['adexturl13'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 13));
		/*header('Location: '.$_SERVER['REQUEST_URI']);*/
		/*header("Location: wp-admin/ad-mgnt.php");*/
		my_refresh($_SERVER['REQUEST_URI']);
	}
	if(isset($_POST['submit14'])){
		$inturl = $_POST['adinturl14'];
		$exturl = $_POST['adexturl14'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 14));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit15'])){
		$inturl = $_POST['adinturl15'];
		$exturl = $_POST['adexturl15'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 15));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit16'])){
		$inturl = $_POST['adinturl16'];
		$exturl = $_POST['adexturl16'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 16));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit17'])){
		$inturl = $_POST['adinturl17'];
		$exturl = $_POST['adexturl17'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 17));
		my_refresh($_SERVER['REQUEST_URI']);
	}
		if(isset($_POST['submit18'])){
		$inturl = $_POST['adinturl18'];
		$exturl = $_POST['adexturl18'];
		$result = $wpdb->update('wp_ads',array('adinturl'=> $inturl,'adexturl'=>$exturl),array('adid'=> 18));
		my_refresh($_SERVER['REQUEST_URI']);
	}

?>

</div>