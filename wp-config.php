<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_avbuzz');

/** MySQL database username */
define('DB_USER', 'wordpress_avbuzz');

/** MySQL database password */
define('DB_PASSWORD', '90fg90dfn9gnggg');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V=rK~l`$>*S2pUw(Nma0ECC,M/E^@4L!~_wCx3I)+&k H Dg[}.%6$KXhVSo!l9}');
define('SECURE_AUTH_KEY',  'Ct(yy!X6*#XC>$H)/nejaTBmes&]={}/!E3<F7L*l!&%gR_{ZUV2Vl^f~]j%l|2v');
define('LOGGED_IN_KEY',    'afU8<wi&!bOzu31}JSk8_4e J<Y/Td+ODx&S[6~H&>k dt.0 >Lne4qk,L2jVjY6');
define('NONCE_KEY',        'MHyeh=.6Ld~0?_3f_%EQW0y)`-HS`cM[_:43i)%*ukRRP*azP5:8)IG(>r* @.iO');
define('AUTH_SALT',        'VwrQq6kFwMxs%3SmARQ!9TcJ-d0h`{!~r)Q1HW31QRF[_r:5yF/qeC~I25(y2Ump');
define('SECURE_AUTH_SALT', 'k1lFhS4Z[P){sc_,~(wfbWu*u:@Wx7l=8qThLYi-UI_o-oR?%^(39`I C8MG;J*<');
define('LOGGED_IN_SALT',   ' II9cR-9.-!/}/5a^W~vU2*el4wm&700B1):>jU- @Ae!C2w;nL.y^yZ~(RKEV4Z');
define('NONCE_SALT',       'N[sYQa5Ai^bJi8-/x6wF)11$9(RMv$m%ec=mq4F5hFdl.2h*#TxkUsxegu.0]o*v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
